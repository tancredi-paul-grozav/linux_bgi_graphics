#!/bin/bash
# ============================================================================ #
# Author: Tancredi-Paul Grozav <paul@grozav.info>
# ============================================================================ #
# Make sure the libs are compiled
# XBGI
# Compile:
(cd /mnt/libs/xbgi/src && make) &&
# Clean:
# (cd /mnt/libs/xbgi/src && make clean && rm libXbgi.a) &&

# sdl-bgi
# Compile:
(cd /mnt/libs/sdl-bgi/src && make) &&
# Clean:
# (cd /mnt/libs/sdl-bgi/src && make clean) &&
# ============================================================================ #
cd /mnt/project &&
# Hardcoded in local ./graphics.h
xbgi_include_path=/mnt/libs/xbgi/src &&
xbgi_library_path=${xbgi_include_path}/libXbgi.a &&
sdl_bgi_library_path=/mnt/libs/sdl-bgi/src/libSDL_bgi.so &&
sdl_bgi_include_path=/mnt/libs/sdl-bgi/src &&
#compile_args="-I${xbgi_include_path}" &&
#compile_args="-I${sdl_bgi_include_path}" &&
compile_args="-Wno-write-strings -I." &&

# Compile
g++ -c main.cpp ${compile_args} -o main.o &&

# Link
g++ ${xbgi_library_path} ${sdl_bgi_library_path} -lX11 -lm -o main \
main.o \
 &&

# Cleanup
rm -f main.o

# Run
./main

exit 0
# ============================================================================ #
