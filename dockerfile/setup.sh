#!/bin/bash
# ============================================================================ #
# Author: Tancredi-Paul Grozav <paul@grozav.info>
# ============================================================================ #
apt-get update &&
# - libx11-dev - required to build xbgi.
# - libsdl2-dev - required to build sdl-bgi
# - g++ - to compile code
# - make - to automate project compilation
apt-get install -y libx11-dev libsdl2-dev g++ make &&

# XBGI
# Compile:
#(cd /mnt/libs/xbgi/src && make && make clean) &&
# Clean:
# (cd /mnt/libs/xbgi/src && make clean && rm libXbgi.a) &&

# sdl-bgi
# Compile:
#(cd /mnt/libs/sdl-bgi/src && make) &&
# Clean:
# (cd /mnt/libs/sdl-bgi/src && make clean) &&

# Compile and run project
#cd /mnt/project && ./compile.sh && ./main &&

exit 0
# ============================================================================ #
