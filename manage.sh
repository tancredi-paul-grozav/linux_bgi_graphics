#!/bin/bash
# ============================================================================ #
# Author: Tancredi-Paul Grozav <paul@grozav.info>
# ============================================================================ #
#set -x && # Start debugging
current_dir="$(cd $(dirname $0) ; pwd)" &&
image_name="linux_bgi_graphics" &&
image_version="1" &&
container_name="${image_name}_"${image_version} &&

# ============================================================================ #
# Build docker image
# ============================================================================ #
function image_build() {(
  cd ${current_dir}/dockerfile &&
  docker build --no-cache -t ${image_name}:${image_version} . -f Dockerfile
)}





# ============================================================================ #
# Remove docker image
# ============================================================================ #
function image_remove() {
  docker image rm ${image_name}:${image_version}
}





# ============================================================================ #
# Run container from docker image
# ============================================================================ #
function container_run() {
  # Local
  # xhost +local:root && docker run -it --rm \
  # -v /tmp/.X11-unix:/tmp/.X11-unix:rw -e DISPLAY debian bash \
  # -c "apt update && apt install -y x11-apps && xeyes" ; xhost -local:root

  # Local with net=host and without xhost permissions
  # (also works over ssh -X with X11 fwd)
  # docker run -it --rm -v ${HOME}/.Xauthority:/root/.Xauthority -e DISPLAY \
  # --net host debian bash -c "apt update && apt install -y x11-apps && xeyes"

  hypervisor_user_id="$(id -u)"
  hypervisor_group_id="$(id -g)"
  hypervisor_user_name="$(whoami)"
  # Could be more than 1 group. see: id -G and id -Gn
  hypervisor_group_name="$(id -Gn | cut -f 1 -d " ")"
  hypervisor_time_zone="$(timedatectl | grep "Time zone:" | awk '{print $3}')"

  # Allow X connection from local
  xhost +local:root &&
  # Start container
  docker run \
    -it \
    --rm \
    --name ${container_name} \
    --volume ${current_dir}/fs/entrypoint:/entrypoint \
    --volume ${current_dir}/fs/mnt:/mnt \
    --volume /tmp/.X11-unix:/tmp/.X11-unix:rw \
    --hostname ${container_name} \
    --env DISPLAY \
    --env TZ="${hypervisor_time_zone}" \
    --env hypervisor_user_id="${hypervisor_user_id}" \
    --env hypervisor_user_name="${hypervisor_user_name}" \
    --env hypervisor_group_id="${hypervisor_group_id}" \
    --env hypervisor_group_name="${hypervisor_group_name}" \
    ${image_name}:${image_version} \
    /bin/bash /mnt/project/compile.sh \
    ;
#    debian:10.2 ;
  xhost -local:root
#    --volume ${HOME}/.ssh/id_rsa:/hypervisor/.ssh/id_rsa \
#    --volume ${HOME}/.ssh/id_rsa.pub:/hypervisor/.ssh/id_rsa.pub \
#    --net host # We don't want to do this because VPN would affect all machine
}





# ============================================================================ #
# Remove and build image.
# ============================================================================ #
function image_rebuild() {
  image_remove && image_build
}





# ============================================================================ #
# Print help
# ============================================================================ #
function print_help() {
  echo "--image_build     Build image." &&
  echo "--image_remove    Remove image." &&
  echo "--image_rebuild   Remove and rebuild image." &&
  echo "--container_run   Run container." &&
  echo "--help            Print the help message."
}





# ============================================================================ #
# Case logic
# ============================================================================ #
# If no parameter
if [ $# == 0 ]; then
  print_help
fi &&

# Case
if [ $1 ]; then
  case "$1" in
    --image_build) image_build ; exit $? ;;
    --image_remove) image_remove ; exit $? ;;
    --image_rebuild) image_rebuild ; exit $? ;;
    --container_run) container_run ; exit $? ;;
    --help) print_help ; exit $? ;;
    *) print_help ; exit $? ;;
    esac
fi
#set +x && # Stop debugging
exit 0
# ============================================================================ #
